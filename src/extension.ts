declare const imports: any;
const Main = imports.ui.main;
const Me = imports.misc.extensionUtils.getCurrentExtension();

const { Gio } = imports.gi;

import * as Log from './modules/log';
import * as Panel from './modules/panel';

import { IEnableableModule } from './interfaces/iEnableableModule';

export class Extension implements IEnableableModule {
  private gpuModeIndicator: any = null;
  private quickToggles: any = null;
  private systemMenu: any = null;

  /**
   * GJS enable() implementation - enables the extension (called by GS)
   * @returns {boolean} enable state
   */
  public enable(): boolean {
    Gio.Resource.load(
      `${Me.path}/resources/org.gnome.Shell.Extensions.supergfxctl-gex.gresource`
    )._register();

    this.systemMenu = Main.panel.statusArea.quickSettings;

    if (typeof this.systemMenu == 'undefined') {
      Log.raw('init', 'system menu is not defined');
      return false;
    }

    this.gpuModeIndicator = new Panel.gpuModeIndicator();
    this.quickToggles = new Panel.gpuModeToggle(this.gpuModeIndicator);

    // must be populated in any case!
    this.gpuModeIndicator.quickSettingsItems.push(this.quickToggles);
    this.enableSystemMenu();

    return true;
  }

  /**
   * enables the system (indicator) menu
   */
  private enableSystemMenu(): void {
    this.systemMenu._indicators.remove_child(this.systemMenu._system);
    this.systemMenu._indicators.add_child(this.gpuModeIndicator);
    this.systemMenu._indicators.add_child(this.systemMenu._system);

    // insert above Background-Processes
    this.systemMenu._addItems(this.gpuModeIndicator.quickSettingsItems);
    for (const item of this.gpuModeIndicator.quickSettingsItems) {
      this.systemMenu.menu._grid.set_child_below_sibling(
        item,
        this.systemMenu._backgroundApps.quickSettingsItems[0]
      );
    }
  }

  /**
   * disables/unloads the extension
   */
  public disable(): void {
    this.quickToggles?.disable();
    this.gpuModeIndicator?.destroy();
    this.quickToggles = null;
    this.gpuModeIndicator = null;
  }
}

/**
 * main initiator, called by GS (extension)
 * @returns Extension instance (myself)
 */
export function init(): Extension {
  return new Extension();
}
