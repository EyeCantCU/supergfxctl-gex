declare const imports: any;
// @ts-ignore: Me must be declared (even if unused) to allow external imports
const Me = imports.misc.extensionUtils.getCurrentExtension();

import { VersionHelper, VersionNumber } from './version_helper';

export enum LabelType {
  Gfx,
  GfxMenu,
  Power,
  PowerFileName,
  UserAction,
}

export class LabelHelper {
  private _gfxLabels: string[] = [
    'hybrid',
    'integrated',
    'vfio',
    'egpu',
    'asusmuxdiscreet',
    'none',
  ];
  private _gfxLabelsMenu: string[] = [
    'Hybrid',
    'Integrated',
    'VFIO',
    'eGPU',
    'MUX / dGPU',
    'None',
  ];
  private _powerLabel: string[] = [
    'active',
    'suspended',
    'off',
    'disabled',
    'active',
    'unknown',
  ];
  private _powerLabelFilename: string[] = [
    'active',
    'suspended',
    'off',
    'off',
    'active',
    'active',
  ];
  private _userAction: string[] = [
    'logout',
    'reboot',
    'integrated',
    'asusgpumuxdisable',
    'none',
  ];
  private vHelper!: VersionHelper;

  /**
   * constructor
   * @param {VersionHelper | VersionNumber} vInstatce  VersionHelper OR VersionNumber instance
   */
  constructor(vInstatce: VersionHelper | VersionNumber) {
    if (vInstatce instanceof VersionHelper) this.vHelper = vInstatce;
    else this.vHelper = new VersionHelper(vInstatce);
  }

  /**
   * @returns {number} gnome-shell version (encapsulated)
   */
  public get gsVersion(): number {
    return this.vHelper.gsVersion();
  }

  /**
   * returns gfx labels for specific type depending on the actual supergfxctl version
   * @param {LabelType} type labeltype to request
   * @returns {string[]} array of corresponding labels (empty on error)
   */
  public gfxLabels(type: LabelType): string[] {
    if (
      !(type === LabelType.Gfx || type === LabelType.GfxMenu) ||
      !this.vHelper.isBetween(
        this.vHelper.currentVersion,
        this.vHelper.allowedSgfxVersions
      )
    ) {
      return []; // unsupported
    }

    switch (type) {
      case LabelType.Gfx:
        return this._gfxLabels
          .slice(0, 2) // first 2 items
          .concat(
            this.v50 ? [] : this.v51 ? ['nvidianomodeset'] : [], // addition (at pos: 3)
            this._gfxLabels.slice(-4) // concatiate the rest
          );
      case LabelType.GfxMenu:
        return this._gfxLabelsMenu
          .slice(0, 2)
          .concat(
            this.v50 ? [] : this.v51 ? ['Nvidia (no modeset)'] : [],
            this._gfxLabelsMenu.slice(-4)
          );
      default:
        return []; // empty
    }
  }

  /**
   * @returns {string[]} list of user actions depending on the actual supergfxctl version
   */
  public get userActions(): string[] {
    return this._userAction
      .slice(0, 1) // first item
      .concat(
        this.v50 ? [] : this.v51 ? ['reboot'] : [], // addition (at pos: 2)
        this._userAction.slice(-3) // concatiate the rest
      );
  }

  /**
   * Get defined label
   * @param {LabelType} type to aquire from
   * @param {number} idx index (number)
   * @returns {string} empty string if not found
   */
  public get(type: LabelType, idx: number): string {
    switch (type) {
      case LabelType.Gfx:
      case LabelType.GfxMenu:
        return this.gfxLabels(type)[idx];
      case LabelType.Power:
        return this._powerLabel[idx];
      case LabelType.PowerFileName:
        return this._powerLabelFilename[idx];
      case LabelType.UserAction:
        return this.userActions[idx];
      default:
        return '';
    }
  }

  /**
   * checks if a requested label equals a provided comparsion string
   * @param {Labeltype} type to aquire from
   * @param {number} idx index
   * @param {string} comp string to compare with
   * @returns {boolean} true if match
   */
  public is(type: LabelType, idx: number, comp: string): boolean {
    return this.get(type, idx) === comp;
  }

  // supported versions
  public get v50(): boolean {
    return this.vHelper.isBetween(this.vHelper.currentVersion, [
      [5, 0, 0],
      [5, 0, 9999],
    ]);
  }
  public get v51(): boolean {
    return this.vHelper.isBetween(this.vHelper.currentVersion, [
      [5, 1, 0],
      [5, 1, 9999],
    ]);
  }
}
