declare const imports: any;
// @ts-ignore: Me must be declared (even if unused) to allow external imports
const Me = imports.misc.extensionUtils.getCurrentExtension();
const Util = imports.misc.util;
const { main, messageTray } = imports.ui;

import * as Resources from './resource_helpers';

export class PanelHelper {
  /**
   * shows a notification
   * @param {string} details notification message
   * @param {string} icon icon (as string)
   * @param {string} action should be empty by default
   * @param {number} urgency the urgency-level of the notification
   */
  public static notify(
    details: string,
    icon: string,
    action: string = '',
    urgency: number = 2
  ): void {
    const params = { gicon: Resources.Icon.getByName(icon) };
    const source = new messageTray.Source(
      'Super Graphics Control',
      icon,
      params
    );
    const notification = new messageTray.Notification(
      source,
      'Super Graphics Control',
      details,
      params
    );

    main.messageTray.add(source);
    notification.setTransient(true);

    switch (action) {
      case 'logout':
        notification.setUrgency(3);
        notification.addAction('Log Out Now!', () => {
          Util.spawnCommandLine('gnome-session-quit');
        });
        break;
      case 'reboot':
        notification.setUrgency(3);
        notification.addAction('Reboot Now!', () => {
          Util.spawnCommandLine('shutdown -ar now');
        });
        break;
      default:
        notification.setUrgency(urgency);
    }

    source.showNotification(notification);
  }
}
