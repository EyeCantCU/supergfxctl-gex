/**
 * Interface for modules which must implement unload patterns
 */
export interface IEnableableModule {
  enable(): void;
  disable(): void;
}
